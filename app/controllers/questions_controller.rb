class QuestionsController < ApplicationController

	def show
		@title = Title.all
	end

	def viewQuestion
		@title = Title.all
	end

	def answerQuestion
		@score = Score.new
		@title = Title.all
	end

	def new
		@question = Question.new
		@title = Title.all
	end

	def saveScore
    # render plain: params.inspect
    params[:answer_quiz].each do |quiz|
      # puts "quiz_id ="+params[:answer_quiz][quiz].inspect + params[:answer_quiz][quiz].inspect
      # @data = Array.new({
      # @data ={
      #   question_id: quiz[11..-1],
      #   score_num: params[:answer_quiz][:"#{quiz}"]
      # }
      # puts @data
      # @score = Score.new({question_id: quiz[11..-1], score_num: params[:answer_quiz][:"#{quiz}"]})
      @title = Title.all
      @score = Score.new
      @score.question_id = quiz[11..-1]
      @score.score_num = params[:answer_quiz][:"#{quiz}"]

     @score.save

    end
    redirect_to viewScore_path
  end

  def viewScore
  	@title = Title.all
  end

  def create
    # render plain: params[:question].permit(:question_text).inspect
    # render plain:  params[:question][:numSec].inspect

    num = params[:question][:numberSec].count

    (1..num).each do |number|
    	@title = Title.new(title: params[:question][:"title#{number}"].to_s)
    	if @title.save
    		params[:question][:"question_text#{number}"].each do |qt|
    			@title.questions << Question.create(question_text: qt)
    		end

    	else
    		render 'new'
    	end
    end
    redirect_to show_questions_path
  end

  def update
    # render plain: params.inspect
    @question = Question.find(params[:id])
    @question.question_text = params[:question][:question_text]

    if @question.save
    	render json: {:status => "OK" }
    	flash[:notice] = 'update success'
    end

  end

  def destroy
  	@question = Question.find(params[:id])
  	if @question.destroy

  		redirect_to show_questions_path

  	else
  		redirect_to show_questions_path
  	end
  end

  # destroy title & questions
  def destroyTitle
  	@title = Title.find(params[:id])

  	if @title.questions.destroy(@title.questions)
  		@title.destroy

  		redirect_to show_questions_path
  	end
  end

  private

  def question_params
  	params.require(:question).permit(:id, :question_text)
  end
end
