Rails.application.routes.draw do

  root to: 'dashboards#index'
  resources :dashboards, :courses, :questions

  get '/show/questions' => 'questions#show'

  delete '/destroyTitle/questions/:id', to: 'questions#destroyTitle', as: 'destroyTitle'
  get '/viewQuestion/questions', to: 'questions#viewQuestion'
  get '/answerQuestion/questions', to: 'questions#answerQuestion', as: 'answerQuestion'
  post '/questions/saveScore/', to: 'questions#saveScore'
  get '/viewScore/questions', to: 'questions#viewScore', as: 'viewScore'

  # post '/questions/:id' => 'questions#update'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
