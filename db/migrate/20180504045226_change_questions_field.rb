class ChangeQuestionsField < ActiveRecord::Migration[5.1]
	def change
		rename_column :questions, :questions, :question_text
	end
end
