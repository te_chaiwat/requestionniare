class CreateScores < ActiveRecord::Migration[5.1]
  def change
    create_table :scores do |t|
      t.integer :question_id
      t.integer :score_num

      t.timestamps
    end
  end
end
