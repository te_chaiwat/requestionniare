class ChangeScoreNumTypeInScores < ActiveRecord::Migration[5.1]
  def change
      change_column :scores, :score_num, :text
  end
end
